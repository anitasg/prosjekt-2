module.exports = {
    parser: '@typescript-eslint/parser', // Specifies ESLint parser
    parserOptions: {
        ecmaVersion: 2020, // Allows parsing modern ECMAScript
        sourceType: 'module', // Allows imports
        ecmaFeatures: {
            jsx: true, // Allows parsing of JSX
        },
    },
    settings: {
        react: {
            version: 'detect', // eslint-plugin-react automatically detects version of React to use
        },
    },
    extends: ['plugin:react/recommended', 'plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
    rules: {
        // Specify ESLint rules

        // Fixes [eslint] Delete 'cr' [prettier/prettier] error
        'prettier/prettier': [
            'error',
            {
                endOfLine: 'auto',
            },
        ],
    },
};

// JavaScript is used for the .eslintrc file (instead of JSON) as it supports comments that can be used to better describe rules
