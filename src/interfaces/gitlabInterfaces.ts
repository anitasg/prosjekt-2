export interface GitlabData {
    commits: Commit[];
    //issues: Issue[];
    //branches: Branch[];
    //mergeRequests: MergeRequest[];
}

export interface Commit {
    id: string;
    short_id: string;
    title: string;
    message: string;
    created_at: Date;
}

export interface Milestone {
    iid: number;
    title: string;
}

export interface Issue {
    id: number;
    iid: number;
    title: string;
    web_url: string;
    labels: string[];
    created_at: Date;
    updated_at: Date;
    milestone: Milestone;
}

export interface Branch {
    name: string;
}

export interface MergeRequest {
    state: string;
}
