import axios, { AxiosResponse } from 'axios';
import { Issue, Commit, GitlabData, Branch, MergeRequest, Milestone } from '../interfaces/gitlabInterfaces';

const baseUrl = 'https://gitlab.stud.idi.ntnu.no/api/v4/projects/';

export interface GitlabApiSettings {
    projectId: number;
    accessToken: string;
}

export const baseApiSettings: GitlabApiSettings = {
    projectId: 11920,
    accessToken: 'XTfFmHqCvhZ8KEsfx_Li',
};

export function goodKey(settings: GitlabApiSettings) {
    return get(settings, '/version');
}

export function getIssues(settings: GitlabApiSettings): Promise<AxiosResponse<RawIssue[]>> {
    return get(settings, '/issues');
}
export interface RawIssue {
    id: number;
    iid: number;
    title: string;
    web_url: string;
    labels: string[];
    created_at: string;
    updated_at: string;
    milestone: Milestone;
}

export function rawIssueToIssue(issue: RawIssue): Issue {
    return {
        ...issue,
        created_at: new Date(issue.created_at),
        updated_at: new Date(issue.updated_at),
    };
}

export function getCommits(settings: GitlabApiSettings): Promise<AxiosResponse<RawCommit[]>> {
    return get(settings, '/repository/commits');
}

export interface RawCommit {
    id: string;
    short_id: string;
    title: string;
    message: string;
    created_at: string;
}

export function rawCommitToCommit(commit: RawCommit): Commit {
    return {
        ...commit,
        created_at: new Date(commit.created_at),
    };
}

export function getBranches(settings: GitlabApiSettings): Promise<AxiosResponse<Branch[]>> {
    return get(settings, '/repository/branches');
}

export function getMergeRequests(settings: GitlabApiSettings): Promise<AxiosResponse<MergeRequest[]>> {
    return get(settings, '/merge_requests');
}

//removed data besides commits to reduce requests
export async function getGitlabData(settings: GitlabApiSettings): Promise<GitlabData> {
    //const issues = getIssues(settings);
    const commits = getCommits(settings);
    //const branches = getBranches(settings);
    //const mergeRequests = getMergeRequests(settings);

    return {
        //issues: (await issues).data.map(rawIssueToIssue),
        commits: (await commits).data.map(rawCommitToCommit),
        //branches: (await branches).data,
        //mergeRequests: (await mergeRequests).data,
    };
}

function get(settings: GitlabApiSettings, url: string) {
    return axios.get(baseUrl + settings.projectId + url, {
        headers: { 'PRIVATE-TOKEN': settings.accessToken },
        params: { all: true, per_page: 100 },
    });
}
