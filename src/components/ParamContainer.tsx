import React, { ReactElement, useEffect, useState } from 'react';
import { GitlabData } from '../interfaces/gitlabInterfaces';
import EndDateInput from './paramInputComponents/EndDate';
import StartDateInput from './paramInputComponents/StartDate';

export interface ParamDescriptor {
    startDate?: boolean;
    endDate?: boolean;
}

export type Param = (data: GitlabData) => GitlabData;
export type ParamInputProps = { onInputChange: (param: Param) => void };
export type ParamInputComponent = React.FC<ParamInputProps>;

interface ParamContainerProps {
    paramDescriptors?: ParamDescriptor;
    onChangedParams: (params?: Param[]) => void;
}

const ParamContainer: React.FC<ParamContainerProps> = (props: ParamContainerProps) => {
    const [params, setParams] = useState<Record<string, Param>>({});

    useEffect(() => {
        props.onChangedParams(Object.values(params));
    }, [params]);

    const paramInputs = [
        {
            id: 'startDate',
            element: <StartDateInput onInputChange={(fn) => onInputChange('startDate', fn)}></StartDateInput>,
        },
        {
            id: 'endDate',
            element: <EndDateInput onInputChange={(fn) => onInputChange('endDate', fn)}></EndDateInput>,
        },
    ];

    const onInputChange = (key: string, param: Param) => {
        setParams({ ...params, [key]: param });
    };

    const getActiveParamElements = () => {
        return paramInputs.filter((param) => {
            const shouldRender =
                Object.entries(props.paramDescriptors ?? {}).filter((entry) => entry[0] === param.id)[0] ?? false;

            return shouldRender;
        });
    };

    return (
        <>
            {getActiveParamElements().map((param: { id: string; element: ReactElement }) => (
                <div key={param.id}>{param.element}</div>
            ))}
        </>
    );
};

export default ParamContainer;
