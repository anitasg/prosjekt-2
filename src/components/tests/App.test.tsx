import React from 'react';
import renderer from 'react-test-renderer';
import App from '../../App';

describe('The App', () => {
    it('should match the snapshot', () => {
        const tree = renderer.create(<App></App>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
