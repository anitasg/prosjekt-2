import React from 'react';
import renderer from 'react-test-renderer';
import ChangeAPIEndpoint from '../ChangeApiEndpoint';

describe('A ChangeApiEndpoint component', () => {
    it('should match the snapshot', () => {
        const tree = renderer
            .create(
                <ChangeAPIEndpoint
                    onNewEndpoint={(a) => {
                        void a;
                    }}
                ></ChangeAPIEndpoint>,
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
