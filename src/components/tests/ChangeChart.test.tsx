import React from 'react';
import renderer from 'react-test-renderer';
import ChangeChart from '../ChangeChart';

describe('A ChangeChart component', () => {
    it('should match the snapshot', () => {
        const tree = renderer.create(<ChangeChart onChartSelect={(desc) => console.log(desc)}></ChangeChart>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
