import React from 'react';
import renderer from 'react-test-renderer';
import VisualizationWrapper from '../VisualizationWrapper';

describe('A VisualizationWrapper component', () => {
    it('should match the snapshot', () => {
        const tree = renderer.create(<VisualizationWrapper></VisualizationWrapper>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
