import React from 'react';
import renderer from 'react-test-renderer';
import ParamContainer, { ParamDescriptor } from '../ParamContainer';

describe('A ParamContainer component', () => {
    it('should match the snapshot on empty params', () => {
        const tree = renderer.create(<ParamContainer onChangedParams={console.log}></ParamContainer>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('should match the snapshot when given params', () => {
        const desc: ParamDescriptor = { startDate: true, endDate: true };
        const tree = renderer
            .create(<ParamContainer paramDescriptors={desc} onChangedParams={console.log}></ParamContainer>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
