import React from 'react';
import renderer from 'react-test-renderer';
import { GitlabDataContext } from '../../App';
import { GitlabData } from '../../interfaces/gitlabInterfaces';
import Visualizer, { Visualizable } from '../Visualizer';

export const gitlabDataMock: GitlabData = {
    commits: [
        {
            created_at: new Date(100000),
            id: '543wq',
            message: 'test message 1',
            short_id: '543wq',
            title: 'test message 1',
        },
        {
            created_at: new Date(110000),
            id: '543wq',
            message: 'test message 1',
            short_id: '543wq',
            title: 'test message 1',
        },
        {
            created_at: new Date(120000),
            id: '543wq',
            message: 'test message 1',
            short_id: '543wq',
            title: 'test message 1',
        },
        {
            created_at: new Date(130000),
            id: '543wq',
            message: 'test message 1',
            short_id: '543wq',
            title: 'test message 1',
        },
    ],
    issues: [],
    branches: [],
    mergeRequests: [],
};

describe('A Visualizer component', () => {
    it('should match the snapshot when no visualizable are supplied', () => {
        const tree = renderer.create(<Visualizer></Visualizer>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('should match snapshot when passed a visualizable', () => {
        const visualizable: Visualizable = {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            formatData: (_data?: GitlabData) => [
                ['a', 20],
                ['t', 2],
                ['g', 8],
            ],
        };
        const data: GitlabData = {
            commits: [],
            issues: [],
            branches: [],
            mergeRequests: [],
        };
        const tree = renderer
            .create(
                <GitlabDataContext.Provider value={data}>
                    <Visualizer visualizer={visualizable}></Visualizer>
                </GitlabDataContext.Provider>,
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
