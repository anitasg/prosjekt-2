import { settings } from 'cluster';
import React, { ChangeEvent, useEffect } from 'react';
import { GitlabApiSettings, goodKey } from '../api/gitlab';
import './ChangeApiEndpoint.css';

interface ChangeAPIEndpointProps {
    onNewEndpoint: (arg0: GitlabApiSettings) => void;
}

const ChangeAPIEndpoint: React.FC<ChangeAPIEndpointProps> = (props: ChangeAPIEndpointProps) => {
    const [id, setId] = React.useState<number>(0);
    const [apiKey, setApiKey] = React.useState<string>('');

    const onButtonClick = () => {
        props.onNewEndpoint({ projectId: id, accessToken: apiKey });
        sessionStorage.setItem('id', id.toString());
        sessionStorage.setItem('accessKey', apiKey);
    };

    const onIdChange = (event: ChangeEvent<HTMLInputElement>) => setId(parseInt(event.target.value));
    const onAPIKeyChange = (event: ChangeEvent<HTMLInputElement>) => setApiKey(event.target.value);

    function getSessionStorage() {
        const storedId = sessionStorage.getItem('id');
        const storedKey = sessionStorage.getItem('accessKey');
        return [storedId, storedKey];
    }

    useEffect(() => {
        const [idFromStorage, apiKeyFromStorage] = getSessionStorage();
        setId(idFromStorage ? parseInt(idFromStorage) : 11920);
        setApiKey(apiKeyFromStorage ? apiKeyFromStorage : 'XTfFmHqCvhZ8KEsfx_Li');
    }, []);

    return (
        <div className="container">
            <label htmlFor="id">Project id:</label> <input type="text" value={id || ''} onChange={onIdChange} id="id" />
            <label htmlFor="api">API key:</label>
            <input type="text" value={apiKey} onChange={onAPIKeyChange} id="api" />
            <button style={{ backgroundColor: '#06d6a0' }} onClick={onButtonClick}>
                Set new API endpoint
            </button>
        </div>
    );
};

export default ChangeAPIEndpoint;
