import React, { useState } from 'react';
import ChangeChart, { ChartDescriptor } from './ChangeChart';
import ParamContainer, { ParamDescriptor } from './ParamContainer';
import Visualizer, { Visualizable } from './Visualizer';
import { Param } from './ParamContainer';
import './VisualizationWrapper.css';

const VisualizationWrapper: React.FC = ({}) => {
    const [params, setParams] = useState<Param[]>([]);
    const [activeParams, setActiveParams] = useState<ParamDescriptor>({});
    const [visualizer, setVisualizer] = useState<Visualizable>();

    const onChartSelect = (chart: ChartDescriptor) => {
        setActiveParams(chart.paramDescriptors);
        setVisualizer(chart.visualizer);
    };

    return (
        <>
            <div className="changeChartContainer">
                <h3 style={{ paddingRight: '5px' }}>Choose data to display:</h3>
                <ChangeChart onChartSelect={(chart) => onChartSelect(chart)}></ChangeChart>
            </div>
            <div className="contentContainer">
                <div className="paramContainer">
                    <h3 style={{ paddingBottom: '40px' }}>Filters:</h3>
                    <ParamContainer
                        onChangedParams={(params) => setParams(params ?? [])}
                        paramDescriptors={activeParams}
                    ></ParamContainer>
                </div>

                <div className="visualizerContainer">
                    <Visualizer params={params} visualizer={visualizer}></Visualizer>
                </div>
            </div>
        </>
    );
};

export default VisualizationWrapper;
