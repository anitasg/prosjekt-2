import { ParamDescriptor } from './ParamContainer';
import { Visualizable } from './Visualizer';
import React, { ReactElement, useEffect } from 'react';
import { GitlabData } from '../interfaces/gitlabInterfaces';
import { Commit } from '../interfaces/gitlabInterfaces';

export interface ChartDescriptor {
    name: string;
    description: string;
    paramDescriptors: ParamDescriptor;
    visualizer: Visualizable;
}

interface ChangeChartProps {
    onChartSelect: (chart: ChartDescriptor) => void;
}

const ChangeChart = (props: ChangeChartProps): ReactElement => {
    const charts: ChartDescriptor[] = [
        {
            name: 'gitmoji usage',
            description:
                'Display a chart showing which gitmojis were most used from commit messages between two chosen dates',
            paramDescriptors: { startDate: true, endDate: true },
            visualizer: {
                formatData: (data?: GitlabData) => {
                    const dict: Record<string, number> = {};
                    data?.commits
                        .filter((commit: Commit) => {
                            if (commit.message.startsWith('Merge')) {
                                return false;
                            } else {
                                return true;
                            }
                        })
                        .map((commit: Commit) => {
                            const name = commit.message.split(':')[1];
                            if (!dict[name]) {
                                dict[name] = 0;
                            }
                            dict[name]++;
                        });
                    delete dict.undefined;
                    return Object.entries(dict).sort((a, b) => b[1] - a[1]);
                },
            },
        },
        {
            name: 'commits per weekday',
            description:
                'Display a chart showing how many ${chart.name} were created each day of the week between two chosen dates',
            paramDescriptors: { startDate: true, endDate: true },
            visualizer: {
                formatData: (data?: GitlabData) => {
                    const daysList = [0, 0, 0, 0, 0, 0, 0];
                    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

                    data?.commits.map(function (commit) {
                        daysList[commit.created_at.getDay()]++;
                    });
                    const weekdaysCommits: Record<string, number> = {};
                    for (let i = 0; i < daysList.length; i++) {
                        weekdaysCommits[weekDays[i]] = daysList[i];
                    }
                    return Object.entries(weekdaysCommits);
                },
            },
        },
    ];

    return (
        <>
            {charts.map((chart) => (
                <button title={chart.description} key={chart.name} onClick={() => props.onChartSelect(chart)}>
                    {chart.name}
                </button>
            ))}
        </>
    );
};

export default ChangeChart;
