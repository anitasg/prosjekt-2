import React, { ChangeEvent, SyntheticEvent, useEffect, useState } from 'react';
import { GitlabData } from '../../interfaces/gitlabInterfaces';
import { ParamInputComponent, ParamInputProps } from '../ParamContainer';

const StartDateInput: ParamInputComponent = (props: ParamInputProps) => {
    const [date, setDate] = useState<string>();
    const onDateInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const inputDate = new Date(value);
        localStorage.setItem('startDate', value);

        const filterFn = (data: GitlabData) => {
            return {
                ...data,
                commits: data.commits.filter(
                    (commit) => commit.created_at.setUTCHours(0, 0, 0, 0) >= inputDate.setUTCHours(0, 0, 0, 0),
                ),
                // issues: data.issues.filter(
                //   (issue) => issue.created_at.setUTCHours(0, 0, 0, 0) >= inputDate.setUTCHours(0, 0, 0, 0),
                //),
            };
        };
        props.onInputChange(filterFn);
        setDate(value);
    };

    useEffect(() => {
        if (localStorage.getItem('startDate'))
            setDate(localStorage.getItem('startDate') || new Date().toLocaleTimeString());
    }, []);

    return (
        <>
            <label>Start date: </label>
            <input type="date" value={date} name="test" id="test" onChange={(e) => onDateInputChange(e)} />
        </>
    );
};

export default StartDateInput;
