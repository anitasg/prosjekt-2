import React, { ChangeEvent } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { GitlabData } from '../../interfaces/gitlabInterfaces';
import { ParamInputComponent, ParamInputProps } from '../ParamContainer';

const EndDateInput: ParamInputComponent = (props: ParamInputProps) => {
    const [date, setDate] = useState<string>();
    const onDateInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const inputDate = new Date(value);
        localStorage.setItem('endDate', value);
        const filterFn = (data: GitlabData) => {
            return {
                ...data,
                commits: data.commits.filter(
                    (commit) => commit.created_at.setUTCHours(0, 0, 0, 0) <= inputDate.setUTCHours(0, 0, 0, 0),
                ),
                // issues: data.issues.filter(
                //   (issue) => issue.created_at.setUTCHours(0, 0, 0, 0) <= inputDate.setUTCHours(0, 0, 0, 0),
                //),
            };
        };
        props.onInputChange(filterFn);
        setDate(value);
    };

    useEffect(() => {
        if (localStorage.getItem('endDate'))
            setDate(localStorage.getItem('endDate') || new Date().toLocaleTimeString());
    }, [date]);

    return (
        <>
            <label>End date: </label>
            <input type="date" value={date} name="test" id="test" onChange={(e) => onDateInputChange(e)} />
        </>
    );
};

export default EndDateInput;
