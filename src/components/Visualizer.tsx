import React from 'react';
import { GitlabDataContext } from '../App';
import { GitlabData } from '../interfaces/gitlabInterfaces';
import { Param } from './ParamContainer';
import { Bar } from 'react-chartjs-2';
import './Visualizer.css';

export type BargraphEntry = [string, number];

export interface Visualizable {
    formatData: (data?: GitlabData) => BargraphEntry[];
}

interface VisualizerProps {
    visualizer?: Visualizable;
    params?: Param[];
}

const Visualizer: React.FC<VisualizerProps> = (VisualizerProps) => {
    const options = {
        maintainAspectRatio: false,
        responsive: true,
        position: 'relative',
        margin: 'auto',
    };

    return (
        <>
            <GitlabDataContext.Consumer>
                {(value) => {
                    if (!value) {
                        return;
                    }
                    let parameterizedData = value;
                    for (const param of VisualizerProps.params ?? []) {
                        parameterizedData = param(parameterizedData);
                    }
                    const inData = VisualizerProps.visualizer?.formatData(parameterizedData);
                    if (!inData) {
                        return <></>;
                    }
                    const barData = {
                        labels: inData?.map((tuple) => tuple[0]),
                        datasets: [
                            {
                                label: 'amount',
                                data: inData?.map((tuple) => tuple[1]),
                                backgroundColor: ['rgba(54, 162, 235, 0.2)'],
                                borderColor: ['rgba(54, 162, 235, 1)'],
                                borderWidth: 1,
                            },
                        ],
                    };

                    return (
                        <div className="barContainer">
                            <Bar data={barData} options={options} />
                        </div>
                    );
                }}
            </GitlabDataContext.Consumer>
        </>
    );
};

export default Visualizer;
