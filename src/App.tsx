import React, { createContext, ReactElement } from 'react';
import './App.css';
import ChangeAPIEndpoint from './components/ChangeApiEndpoint';
import VisualizationWrapper from './components/VisualizationWrapper';
import { getGitlabData, GitlabApiSettings, baseApiSettings } from './api/gitlab';
import { GitlabData } from './interfaces/gitlabInterfaces';

export const GitlabDataContext = createContext<GitlabData | undefined>(undefined);

interface AppState {
    apiEndpoint: GitlabApiSettings;
    gitlabData: GitlabData;
}

class App extends React.Component {
    state: AppState = {
        apiEndpoint: baseApiSettings,
        gitlabData: {
            commits: [],
            // issues: [],
            // branches: [],
            // mergeRequests: [],
        },
    };

    refreshGitlabData(): void {
        getGitlabData(this.state.apiEndpoint)
            .then((data) => this.setState({ gitlabData: data }))
            .catch((e) => window.alert('Could not get gitlab data, ' + e));
    }

    componentDidMount(): void {
        getGitlabData(this.state.apiEndpoint)
            .then((data) => this.setState({ gitlabData: data }))
            .catch((e) => window.alert('Could not get gitlab data, ' + e));
    }

    render(): ReactElement {
        return (
            <div className="App">
                <header className="App-header">
                    <ChangeAPIEndpoint
                        onNewEndpoint={(endpoint) => {
                            this.setState({ apiEndpoint: endpoint });
                            this.refreshGitlabData();
                        }}
                    ></ChangeAPIEndpoint>
                    <GitlabDataContext.Provider value={this.state.gitlabData}>
                        <VisualizationWrapper></VisualizationWrapper>
                    </GitlabDataContext.Provider>
                </header>
            </div>
        );
    }
}

export default App;
