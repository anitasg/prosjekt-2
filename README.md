# Prosjekt 2 - Gruppe 27

## For å kjøre prosjektet

For å kjøre prosjektet for første gang må det brukes **npm install** og **npm start**. Deretter vil applikasjonen kjøre på localhost:3000.

For å kjøre test **npm test**

## Introduksjon til prosjektet

I dette prosjektet er hovedhensikten å visualisere data fra et gitlab-repository. Data som «commits», «issues” og “merge requests” har vært prioritert.

Bilder fra applikasjonen:

### Brukerhistorier

1. Som bruker skal jeg kunne skrive inn «access key» og link til et gitlab-repository, slik at jeg får alle dataene visualisert.

2. Som bruker skal jeg kunne trykke på "commits" og få opp en graf som viser antall commits og hvem som har gjennomført disse "commits".

3. Som bruker skal jeg kunne lukke nettleseren og innstillingen for grafene skal være det samme, slik at jeg slipper å velge på nytt.

4. Som bruker skal jeg kunne forvente at «access key» må fylles inn hver gang jeg åpner nettsiden på nytt.

## Mappestruktur

Følgende mapper utgjør --- av prosjektet.

## Teknologier

I dette prosjektet har gruppen tatt i bruk TypeScript og React for å implementere prosjektet. Typescript er et superset av Javascript og har statisk type. Det er en fordel å bruke TypeScript i gruppeprosjekter for å redusere feil og misforståelser.
React er et programmeringsspråk som transformerer data til brukergrensesnitt. Det tillater utviklere å lage gjenbrukbare komponenter som er uavhengige av hverandre. Dette prosjektet består hovedsakelig av TypeScript og React og gruppen har opplevd programmeringsspråkene som effektive og ryddige.

Prosjektet består hovedsakelig av å visualisere data fra et GitLab-repository og brukeren skal ha mulighet for å velge hvordan data blir presentert. For å oppnå dette har vi importert Axios. Axios er et bibliotek som kan lage http-forespørsler til eksterne ressurser. I prosjektet bruker vi Axios for å hente data fra serveren. Koden for dette ligger i **gitlab.ts**.

Et av tekniske krav for prosjektet er å inkludere Context API. Gruppen fant det naturlig å bruke Context API for å hente GitLab data, fordi dataen brukes av flere komponenter.

HTML Web Storage handler om å lagre data i brukerens nettleser. Local storage innebærer at denne dataen lagres permanent. Her har gruppen implementert local storage når brukeren fyller inn parametre for data som skal vises. Dermed kan brukeren bruke den samme innstillingen for data til neste gang nettsiden besøkes. Session storage handler om at dataen blir lagret for denne økten og slettes når brukeren forlater nettsiden. Gruppen mener at det er hensiktsmessig å implementere dette for å lagre gitlab-repository og access key som brukeren fyller inn. Årsaken er at access key er sensitiv informasjon som ikke bør lagres permanent.

I dette prosjektet har gruppen fått beskjed om å bruke både funksjonelle komponenter og class. Dette er ikke naturlig i virkelige utviklingsprosjekter, men gruppen fikk en dypere forståelse i forskjellene og bruksområdene deres.
Gruppen brukte hovedsakelig funksjonelle komponenter i prosjektet og har App som class komponent. Funksjonelle komponenter skal brukes når den bare skal ta inn «props» og returnere en JSX definisjon og gruppen mener det er hensiktsmessig å bruke disse i prosjektet.

Responsive design er et av tekniske kravene og gruppen har brukt Flex Box for å oppnå dette. I dette prosjektet må nettsiden vise flere containere med grafikk og gruppen mente det var naturlig og mer fleksibelt å bruke Flex Box for å justere plassering av elementene. Nå er prosjektet tilpasset til iPad (), vanlig PC skjerm () og mobilskjerm både horisontal og vertikal. Oppførselen og interaksjonen er like på alle disse skjermene. Prosjektet er også kompatibelt med browsers som Google Chrome, Microsoft Edge og Firefox. Dette har blitt brukertestet av en ekstern student.

## Testing

Jest er et testingsrammeverk i JavaScript for å verifisere og kvalitetssikre kodebasen. For å kontrollere oppførselen til prosjektet har gruppen brukt Jest for å enhetsteste de ulike delene i prosjektet. I tillegg har gruppen importert Snapshottest, som sikrer at brukergrensesnittet oppfører seg som forventet. Det blir tatt et referansesnapshot på hvordan brukergrensesnittet skal se ut, og et annet snapshot på hvordan det faktisk ser ut. Disse snapshots skal sammenlignes og testen mislykkes hvis de ikke samsvarer.

For å oppdatere snapshot **jest –-updateSnapshot**

ESLint har også blitt brukt for å analysere kodekvalitet til syntaksene.

## Annet

I dette prosjektet er det brukt **npm package \_\_\_**
